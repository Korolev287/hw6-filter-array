//  Foreach по очереди перебирает каждый элемент массива(коллекции) и присваивает переменной цикла.

const dataArray = [ 'Яблоко',56 , { name: 'Джон' }, 34, true, function() { alert('привет'); } ];
const dataType = "number";  

function filterBy(array, type) { 
  let dataArrayNew =  array.filter(n => typeof n !== type);
  return dataArrayNew;
};

console.log(filterBy(dataArray, dataType)); 



